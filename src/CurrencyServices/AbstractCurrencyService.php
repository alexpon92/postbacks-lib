<?php
declare(strict_types=1);

namespace PostBackLib\CurrencyServices;

use PostBackLib\CurrencyServices\Exceptions\CurrencyServiceBadResponseCodeException;
use PostBackLib\CurrencyServices\Exceptions\CurrencyServiceBadResponseFormatException;

abstract class AbstractCurrencyService
{

    /**
     * @param string $currencyFrom
     * @param string $currencyTo
     *
     * @return RatesResponseDto
     * @throws CurrencyServiceBadResponseCodeException
     * @throws CurrencyServiceBadResponseFormatException
     */
    abstract public function getRates(string $currencyFrom, string $currencyTo): RatesResponseDto;

    /**
     * @param string $from
     * @param string $to
     * @param string $amount
     *
     * @return string
     * @throws CurrencyServiceBadResponseCodeException
     * @throws CurrencyServiceBadResponseFormatException
     */
    public function convert(string $from, string $to, string $amount): string
    {
        $rates = $this->getRates($from, $to);

        return bcmul($amount, bcdiv($rates->getTo(), $rates->getFrom(), 6), 6);
    }

    protected function formatCurrency(string $currency): string
    {
        return mb_strtoupper(trim($currency));
    }
}