<?php
declare(strict_types=1);

namespace PostBackLib\CurrencyServices;


use GuzzleHttp\Client;
use PostBackLib\CurrencyServices\Exceptions\CurrencyServiceBadResponseCodeException;
use PostBackLib\CurrencyServices\Exceptions\CurrencyServiceBadResponseFormatException;

class FixerApiService extends AbstractCurrencyService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * FixerService constructor.
     *
     * @param string      $apiKey
     * @param Client|null $client
     *
     */
    public function __construct(string $apiKey, ?Client $client = null)
    {
        $this->apiKey = $apiKey;
        $this->client = $client ?? new Client(['http_errors' => false]);
    }

    public function getRates(string $from, string $to): RatesResponseDto
    {
        $from = $this->formatCurrency($from);
        $to   = $this->formatCurrency($to);

        $response = $this->client->get(
            'http://data.fixer.io/api/latest',
            [
                'query' => [
                    'access_key' => $this->apiKey,
                    'symbols'    => "{$from},{$to}"
                ]
            ]
        );

        if (!$response || $response->getStatusCode() !== 200) {
            $statusCode = null !== $response ? $response->getStatusCode() : 'null';
            throw new CurrencyServiceBadResponseCodeException(
                "Bad response code for fixer API $statusCode"
            );
        }

        $content = $response->getBody()->getContents();
        if (!$content || !($data = json_decode($content, true))) {
            throw new CurrencyServiceBadResponseFormatException(
                'Bad response format from fixer API. No content of malformed JSON'
            );
        }

        if (!isset($data['success']) || !$data['success']) {
            throw new CurrencyServiceBadResponseFormatException(
                'Bad response format from fixer API. Request status success is false'
            );
        }

        if (!isset($data['rates'][$from], $data['rates'][$to])) {
            throw new CurrencyServiceBadResponseFormatException(
                'Bad response format from fixer API. No requested currency in response'
            );
        }

        return new RatesResponseDto(
            (string)$data['rates'][$from],
            (string)$data['rates'][$to]
        );
    }
}