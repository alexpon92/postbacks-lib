<?php
declare(strict_types=1);

namespace PostBackLib\CurrencyServices;

class CurrencyServicesFactory
{
    public static function build(string $apiKey): AbstractCurrencyService
    {
        return new FixerApiService($apiKey);
    }
}