<?php
declare(strict_types=1);

namespace PostBackLib\CurrencyServices\Exceptions;

class CurrencyServiceBadResponseCodeException extends \Exception
{

}