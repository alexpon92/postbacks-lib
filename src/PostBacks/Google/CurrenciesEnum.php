<?php
declare(strict_types=1);

namespace PostBackLib\PostBacks\Google;

class CurrenciesEnum
{
    public const AVAILABLE_CURRENCIES = [
        'USD',
        'AED',
        'ARS',
        'AUD',
        'BGN',
        'BOB',
        'BRL',
        'CAD',
        'CHF',
        'CLP',
        'CNY',
        'COP',
        'CZK',
        'DKK',
        'EGP',
        'EUR',
        'FRF',
        'GBP',
        'HKD',
        'HRK',
        'HUF',
        'IDR',
        'ILS',
        'INR',
        'JPY',
        'KRW',
        'LTL',
        'MAD',
        'MXN',
        'MYR',
        'NOK',
        'NZD',
        'PEN',
        'PHP',
        'PKR',
        'PLN',
        'RON',
        'RSD',
        'RUB',
        'SAR',
        'SEK',
        'SGD',
        'THB',
        'TRY',
        'TWD',
        'UAH',
        'VEF',
        'VND',
        'ZAR'
    ];

    /**
     * @param string $curr
     *
     * @return bool
     */
    public static function isInAvailableCurrencies(string $curr): bool
    {
        return in_array(self::formatCurrency($curr), self::AVAILABLE_CURRENCIES, true);
    }

    /**
     * @param string $curr
     *
     * @return string
     */
    public static function formatCurrency(string $curr): string
    {
        return mb_strtoupper(trim($curr));
    }
}