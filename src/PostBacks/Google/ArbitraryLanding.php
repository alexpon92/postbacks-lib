<?php
declare(strict_types=1);

namespace PostBackLib\PostBacks\Google;

use PostBackLib\CurrencyServices\AbstractCurrencyService;
use PostBackLib\CurrencyServices\CurrencyServicesFactory;
use PostBackLib\CurrencyServices\Exceptions\CurrencyServiceBadResponseCodeException;
use PostBackLib\CurrencyServices\Exceptions\CurrencyServiceBadResponseFormatException;
use PostBackLib\PostBacks\Google\Exceptions\IncorrectConversionTypeException;
use PostBackLib\PostBacks\Google\Exceptions\RequiredParamsMissingException;
use TheIconic\Tracking\GoogleAnalytics\Analytics;

class ArbitraryLanding
{
    /**
     * @var Analytics
     */
    private $analyticsClient;

    /**
     * @var string
     */
    private $conversionType;

    /**
     * @var string
     */
    private $defaultCurrency;

    /**
     * @var string
     */
    private $googleTrackingId;

    /**
     * @var AbstractCurrencyService
     */
    private $currencyService;

    /**
     * ArbitraryLanding constructor.
     *
     * @param Analytics               $httpClient
     * @param string                  $conversionType
     * @param string                  $defaultCurrency
     * @param string                  $googleTrackingId
     * @param AbstractCurrencyService $currencyService
     */
    public function __construct(
        Analytics $httpClient,
        string $conversionType,
        string $defaultCurrency,
        string $googleTrackingId,
        AbstractCurrencyService $currencyService
    ) {
        $this->analyticsClient  = $httpClient;
        $this->conversionType   = $conversionType;
        $this->defaultCurrency  = $defaultCurrency;
        $this->googleTrackingId = $googleTrackingId;
        $this->currencyService  = $currencyService;
    }


    public static function build(
        string $conversionType,
        string $defaultCurrency,
        string $trackingId,
        string $currencyServiceApiKey
    ): self {
        return new self(
            new Analytics(),
            $conversionType,
            $defaultCurrency,
            $trackingId,
            CurrencyServicesFactory::build($currencyServiceApiKey)
        );
    }

    /**
     * @param array $queryParams
     *
     * @return string
     * @throws CurrencyServiceBadResponseCodeException
     * @throws CurrencyServiceBadResponseFormatException
     * @throws IncorrectConversionTypeException
     * @throws RequiredParamsMissingException
     */
    public function sendTransaction(array $queryParams): string
    {
        $conversionType = $queryParams['sub1'] ?? '';
        $clientId       = $queryParams['sub5'] ?? '';
        $cpaName        = $queryParams['cpa'] ?? 'undefined';
        $currency       = $queryParams['currency'] ?? $this->defaultCurrency;
        $offerId        = $queryParams['offer_id'] ?? '';
        $offerName      = $queryParams['offer_name'] ?? '';
        $revenue        = $queryParams['revenue'] ?? '';

        if ($currency === 'RUR') {
            $currency = 'RUB';
        }

        if (empty($conversionType) || empty($clientId)) {
            throw new RequiredParamsMissingException('Required params sub1 or sub5 are missing or not set');
        }

        if ($conversionType !== $this->conversionType) {
            throw new IncorrectConversionTypeException("Incorrect conversion type passed {$conversionType}");
        }

        $transactionId  = $this->generateTransactionId();
        $conversionName = "{$offerName}, CPA: {$cpaName}, offer_id: {$offerId}";


        $this->analyticsClient->setProtocolVersion('1')
                              ->setTrackingId($this->googleTrackingId)
                              ->setClientId($clientId);

        if (!CurrenciesEnum::isInAvailableCurrencies($currency)) {
            $revenue  = $this->currencyService->convert($currency, $this->defaultCurrency, $revenue);
            $currency = $this->defaultCurrency;
        }

        $this->analyticsClient->setTransactionId($transactionId)
                              ->setRevenue($revenue)
                              ->setCurrencyCode($currency)
                              ->sendTransaction();


        $this->analyticsClient->setTransactionId($transactionId)
                              ->setItemName($conversionName)
                              ->setItemPrice($revenue)
                              ->setItemQuantity('1')
                              ->setCurrencyCode($currency)
                              ->sendItem();

        return $transactionId;
    }


    private function generateTransactionId(): string
    {
        return time() . random_int(1, 10000000);
    }
}