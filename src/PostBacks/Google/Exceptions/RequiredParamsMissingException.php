<?php
declare(strict_types=1);

namespace PostBackLib\PostBacks\Google\Exceptions;

class RequiredParamsMissingException extends \Exception
{

}